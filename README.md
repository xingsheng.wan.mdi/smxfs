# smxFS


*  Portable FAT File System

smxFS is a FAT file system that is media-compatible with DOS/Windows. It has small code and data footprints, making it ideal for small embedded systems. smxFS supports flash media such as USB thumb drives, CompactFlash, and SD/MMC. It provides the standard C library file API.


*  Product Description

smxFS is a FAT file system for hard real-time embedded systems. It supports fixed and removable media, and offers drivers for the media typically used in modern embedded systems, such as USB thumb drives, CompactFlash, and SD/MMC cards. It is DOS/Windows media-compatible, so that media written by smxFS are interchangeable with Windows and other OSs that support the FAT file system. smxFS requires minimal ROM and RAM, allowing it to be used in very small embedded systems, as well as in larger ones. smxFS supports FAT12/16/32 and VFAT (long file names compatible with Win32 operating systems). It uses the standard C library file API (i.e. fopen(), fread(), etc.) that is familiar to most C programmers, and it is extended with additional functions for other needed operations. smxFS API functions are reentrant so that smxFS is safe for multitasking.